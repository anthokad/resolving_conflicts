BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (pre-release)
$ git merge dev

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (pre-release)
$ git merge release
fatal: refusing to merge unrelated histories

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (pre-release)
$ git merge --allow-unrelated-histories release
Merge made by the 'recursive' strategy.
 Scripts/.gitkeep    | 0
 Scripts/scriptfile1 | 1 +
 Scripts/testfile2   | 2 ++
 3 files changed, 3 insertions(+)
 create mode 100644 Scripts/.gitkeep
 create mode 100644 Scripts/scriptfile1
 create mode 100644 Scripts/testfile2

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (pre-release)
$ git checkout master
Switched to branch 'master'
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master)
$ git merge --
--abort                       --ff-only                     --overwrite-ignore            --stat
--allow-unrelated-histories   --file                        --progress                    --strategy=
--cleanup=                    --gpg-sign                    --quiet                       --strategy-option=
--commit                      --log                         --quit                        --summary
--continue                    --message=                    --rerere-autoupdate           --verbose
--edit                        --no-...                      --signoff                     --verify
--ff                          --no-stat                     --squash                      --verify-signatures

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master)
$ git merge --allow-unrelated-histories pre-release
CONFLICT (add/add): Merge conflict in documents/userguide
Auto-merging documents/userguide
CONFLICT (add/add): Merge conflict in Scripts/testfile2
Auto-merging Scripts/testfile2
Automatic merge failed; fix conflicts and then commit the result.

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
        both added:      Scripts/testfile2
        both added:      documents/userguide

no changes added to commit (use "git add" and/or "git commit -a")

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git checkout --theirs Scripts/testfile2
Updated 1 path from the index

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git checkout --theirs documents/userguide
Updated 1 path from the index

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
        both added:      Scripts/testfile2
        both added:      documents/userguide

no changes added to commit (use "git add" and/or "git commit -a")

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git diff Scripts/testfile2
diff --cc Scripts/testfile2
index 455d7c6,bd6b19d..0000000
--- a/Scripts/testfile2
+++ b/Scripts/testfile2

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git add .

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master|MERGING)
$ git commit -m 'merge-all'
[master bf34a2d] merge-all

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master)
$ git push origin master
remote: You are not allowed to push code to this project.
fatal: unable to access 'https://gitlab.com/anthokad/resolving_conflicts.git/': The requested URL returned error: 403

BuTTerFly@DESKTOP-0UFGT6B MINGW64 /d/GitHub/resolving_conflicts (master)
$ git push origin master
Enumerating objects: 21, done.
Counting objects: 100% (21/21), done.
Delta compression using up to 12 threads
Compressing objects: 100% (14/14), done.
Writing objects: 100% (16/16), 1.61 KiB | 412.00 KiB/s, done.
Total 16 (delta 3), reused 0 (delta 0)
To https://gitlab.com/anthokad/resolving_conflicts.git
   8f15529..bf34a2d  master -> master